/*
 * Poniesbox - ponies on the desktop
 * Copyright (C) 2013 Sorokin Alexei <sor.alexei@meowr.ru>
 * Copyright (C) 2012 mysha <myszha@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://gnu.org/licenses/>.
 */

#ifndef CONFIGWINDOW_H
#define CONFIGWINDOW_H

#include <QtCore>
#include <QtGui>
#ifndef IS_QT4
#include <QtWidgets>
#endif

#include <memory>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <cmath>

#include "pony.h"
#include "interaction.h"

namespace Ui {
    class ConfigWindow;
}

namespace std {
    template <>
    struct hash<pair<QString, QString> >
    {
        size_t operator()(const pair<QString, QString> &p) const
        {
            // Maybe change it so it gives the same hash for <p1,p2> and <p2,p1>
            return qHash(QPair<QString, QString>(p.first, p.second));
        }
    };
}

class ConfigWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ConfigWindow(QWidget *parent = 0);
    ~ConfigWindow();


    std::list<std::shared_ptr<Pony>> ponies;
    QTimer update_timer;
    QTimer interaction_timer;

#ifdef Q_WS_X11
    enum class X11_WM_Types { Unknown, Compiz, KWin };
    X11_WM_Types getX11_WM();
#endif

    static const std::unordered_map<QString, const QVariant> config_defaults;
    static QVariant conf_read(const QString &section, const QString &value, QSettings *settings = NULL);
    static void conf_write(const QString &section, const QString &value, const QVariant &inValue, QSettings *settings = NULL);

private:
    void move_center();
    void reload_available_ponies();
    void update_distances();
    void detect_x11_wm();

    std::vector<Interaction> interactions;
    std::unordered_map<std::pair<QString, QString>, float> distances;

    Ui::ConfigWindow *ui;
    QSignalMapper *signal_mapper;
    QStandardItemModel *list_model;
    QStandardItemModel *active_list_model;
    QSystemTrayIcon tray_icon;
    QMenu tray_menu;

#ifdef Q_WS_X11
    X11_WM_Types x11_wm;
#endif

public slots:
    void remove_pony();
    void remove_pony_all();

private slots:
    void remove_pony_activelist();
    void newpony_list_changed(QModelIndex item);
    void add_pony();
    void update_active_list();
    void toggle_window(QSystemTrayIcon::ActivationReason reason);
    void save_settings();
    void load_settings();
    void lettertab_changed(int index);
    void change_ponydata_directory();
    void update_interactions();
    void receiveFromInstance(const QString &message);
    void closeEvent(QCloseEvent *event = NULL);
};

#endif // CONFIGWINDOW_H
