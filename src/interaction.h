/*
 * Poniesbox - ponies on the desktop
 * Copyright (C) 2013 Sorokin Alexei <sor.alexei@meowr.ru>
 * Copyright (C) 2012 mysha <myszha@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://gnu.org/licenses/>.
 */


#ifndef INTERACTION_H
#define INTERACTION_H

#include <QtCore>

#include <vector>
#include <random>

#include "csv_parser.h"

class Interaction
{
public:
    Interaction(const std::vector<QVariant> &options);

    static const CSVParser::ParseTypes OptionTypes;

    const QString select_behavior();

    QString name;
    QString pony;
    float probability;
    int distance;
    QList<QVariant> targets;
    bool select_every_taget;
    QList<QVariant> behaviors;
    int reactivation_delay;
};

#endif // INTERACTION_H
