<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../src/configwindow.ui" line="14"/>
        <source>Poniesbox</source>
        <translation>Poniesbox</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="63"/>
        <source>Add pony</source>
        <translation>Ajouter un poney</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="220"/>
        <source>Remove pony</source>
        <translation>Supprimer un poney</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="293"/>
        <source>&amp;Main</source>
        <translation>&amp;Principal</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="309"/>
        <source>Show the ponies in half size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="312"/>
        <source>&amp;Small ponies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="345"/>
        <source>Toggles if ponies are always on top of other windows</source>
        <translation>Définit si les poney sont toujours au-dessus des autres fenêtres</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="348"/>
        <source>&amp;Always on top</source>
        <translation>Toujours &amp;au-dessus</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="472"/>
        <source>Hide &amp;tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="322"/>
        <source>Bypass the X11 window manager, showing ponies on every desktop on top of every window</source>
        <translation>Ignorer le gestionnaire de fenêtre X11, affiche les poneys sur chaque bureau au-dessus de chaque fenêtre</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="325"/>
        <source>Show on every &amp;virtual desktop</source>
        <translation>Afficher sur chaque bureau &amp;virtuel</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="388"/>
        <source>Specifies in which directory the pony data and configuration are</source>
        <translation>Spécifie le dossier où les données et la configuration se trouvent</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="391"/>
        <source>Pony data di&amp;rectory</source>
        <translation>Dossier des &amp;données du programme</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="358"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="365"/>
        <source>Toggle if interactions are to be executed</source>
        <translation>Définir si les interactions doivent être éxécutées</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="368"/>
        <source>&amp;Interactions enabled</source>
        <translation>&amp;Interactions activées</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="411"/>
        <source>Toggle if effects are enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="414"/>
        <source>Ef&amp;fects enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="490"/>
        <source>S&amp;peech</source>
        <translation>&amp;Paroles</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="508"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="537"/>
        <source>For how long the speech is to stay on screen</source>
        <translation>Pour combien de temps les paroles doivent rester à l&apos;écran</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="540"/>
        <source>&amp;Text delay</source>
        <translation>Delai du &amp;Texte</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="550"/>
        <source>Toggles if ponies are to speak</source>
        <translation>Définit si les poneys doivent parler</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="553"/>
        <source>&amp;Speech enabled</source>
        <translation>Parole&amp;s activées</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="582"/>
        <source>NOT ACTIVE. Toggles the playing of sounds when a pony speaks.</source>
        <translation>NON ACTIF. Définit le son d&apos;un poney qui parle.</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="585"/>
        <source>Play s&amp;ounds</source>
        <translation>Jouer les s&amp;ons</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="605"/>
        <source>How frequently ponies speak</source>
        <translation>Fréquence à laquelle les poneys parlent</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="608"/>
        <source>Sp&amp;eech probability</source>
        <translation>Probabilité de parol&amp;e</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="621"/>
        <source> %</source>
        <translation>.%</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="262"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="282"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="77"/>
        <source>Open configuration</source>
        <translation>Ouvrir la configuration</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="78"/>
        <source>Close application</source>
        <translation>Fermer le programme</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="29"/>
        <source>Add ponies</source>
        <translation>Ajouter des poneys</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="205"/>
        <source>Active ponies</source>
        <translation>Poneys actifs</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="256"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="425"/>
        <source>Select pony data directory</source>
        <translation>Selectionner le dossier des données des poneys</translation>
    </message>
</context>
<context>
    <name>Pony</name>
    <message>
        <location filename="../src/pony.cpp" line="163"/>
        <source>Sleeping</source>
        <translation>Dormant</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="170"/>
        <source>Remove %1</source>
        <translation>Supprimer %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="171"/>
        <source>Remove every %1</source>
        <translation>Tout supprimer %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="172"/>
        <source>Open configuration</source>
        <translation>Ouvrir la configuration</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="174"/>
        <source>Close application</source>
        <translation>Fermer le programme</translation>
    </message>
</context>
</TS>
