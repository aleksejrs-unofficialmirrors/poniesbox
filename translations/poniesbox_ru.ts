<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../src/configwindow.ui" line="14"/>
        <source>Poniesbox</source>
        <translation>Poniesbox</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="63"/>
        <source>Add pony</source>
        <translation>Добавить пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="220"/>
        <source>Remove pony</source>
        <translation>Удалить пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="293"/>
        <source>&amp;Main</source>
        <translation>&amp;Основные</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="309"/>
        <source>Show the ponies in half size</source>
        <translation>Рисовать пони вдвое меньше</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="345"/>
        <source>Toggles if ponies are always on top of other windows</source>
        <translation>Показывать пони поверх всех окон</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="348"/>
        <source>&amp;Always on top</source>
        <translation>&amp;Поверх всего</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="322"/>
        <source>Bypass the X11 window manager, showing ponies on every desktop on top of every window</source>
        <translation>Обходит оконный менеджер X11, показывая пони на каждом рабочем столе поверх всех окон</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="325"/>
        <source>Show on every &amp;virtual desktop</source>
        <translation>Показывать на всех виртуальных рабочих &amp;столах</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="388"/>
        <source>Specifies in which directory the pony data and configuration are</source>
        <translation>Указывает каталог с конфигурацией и пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="391"/>
        <source>Pony data di&amp;rectory</source>
        <translation>Ка&amp;талог с пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="358"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="312"/>
        <source>&amp;Small ponies</source>
        <translation>&amp;Уменьшить пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="365"/>
        <source>Toggle if interactions are to be executed</source>
        <translation>Переключает исполнение взаимодействий</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="368"/>
        <source>&amp;Interactions enabled</source>
        <translation>&amp;Взаимодействия</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="411"/>
        <source>Toggle if effects are enabled</source>
        <translation>Включение/отключение эффектов</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="414"/>
        <source>Ef&amp;fects enabled</source>
        <translation>Эф&amp;фекты</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="472"/>
        <source>Hide &amp;tray icon</source>
        <translation>Скрыть &amp;значок в трее</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="490"/>
        <source>S&amp;peech</source>
        <translation>&amp;Реплики</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="508"/>
        <source> ms</source>
        <translation> мс</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="537"/>
        <source>For how long the speech is to stay on screen</source>
        <translation>Продолжительность видимости реплик в миллисекундах</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="540"/>
        <source>&amp;Text delay</source>
        <translation>Продолжительность видимости &amp;реплик</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="550"/>
        <source>Toggles if ponies are to speak</source>
        <translation>Включить/выключить реплики пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="553"/>
        <source>&amp;Speech enabled</source>
        <translation>&amp;Включить реплики</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="582"/>
        <source>NOT ACTIVE. Toggles the playing of sounds when a pony speaks.</source>
        <translation>НЕ АКТИВНО. Озвучивать реплики пони.</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="585"/>
        <source>Play s&amp;ounds</source>
        <translation>Во&amp;спроизводить звуки</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="605"/>
        <source>How frequently ponies speak</source>
        <translation>Как часто пони произносят реплики</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="608"/>
        <source>Sp&amp;eech probability</source>
        <translation>Ве&amp;роятность реплик</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="621"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="262"/>
        <source>Accept</source>
        <translation>Подтвердить</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="282"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="77"/>
        <source>Open configuration</source>
        <translation>Открыть окно конфигурации</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="78"/>
        <source>Close application</source>
        <translation>Закрыть приложение</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="29"/>
        <source>Add ponies</source>
        <translation>Добавить пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="205"/>
        <source>Active ponies</source>
        <translation>Активные пони</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="256"/>
        <source>Configuration</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="425"/>
        <source>Select pony data directory</source>
        <translation>Выберите каталог с пони</translation>
    </message>
</context>
<context>
    <name>Pony</name>
    <message>
        <location filename="../src/pony.cpp" line="163"/>
        <source>Sleeping</source>
        <translation>В сон</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="170"/>
        <source>Remove %1</source>
        <translation>Удалить %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="171"/>
        <source>Remove every %1</source>
        <translation>Удалить всех %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="172"/>
        <source>Open configuration</source>
        <translation>Открыть окно конфигурации</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="174"/>
        <source>Close application</source>
        <translation>Закрыть приложение</translation>
    </message>
</context>
</TS>
